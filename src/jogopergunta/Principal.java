/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jogopergunta;

import Animal.Animal;
import Caracteristicas.Caracteristicas;
import Caracteristicas.Primata;
import Generico.AnimalGenerico;
import Generico.CaracteristicaGenerica;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import javax.swing.JOptionPane;

/**
 *
 * @author vinicius
 */
public class Principal {

    private HashMap<Caracteristicas,List<Animal>> lista;
    
    public Principal() {
        Controle controle = new Controle();
        lista = controle.getListaRelativa();
        while(true){
            Set<Caracteristicas> caracteristicas = lista.keySet();
            JOptionPane.showMessageDialog(null, "Pense num animal");
            boolean teste = true;
            for(Caracteristicas c : caracteristicas){
                int res = JOptionPane.showConfirmDialog(null, "Seu animal é "+c.caracteristicas());
                if(res == JOptionPane.YES_OPTION){
                    teste = false;
                    boolean acerto = buscaPorAnimal(c);
                    if(acerto)break;
                }
            }
            if(teste)adicionaCategoria();
            int sair = JOptionPane.showConfirmDialog(null, "Deseja continuar jogando!");
            if(sair != JOptionPane.YES_OPTION)System.exit(0);
        }        
    }
    
    private void adicionaCategoria(){
        String c = JOptionPane.showInputDialog("Qual a caracteristica do animal que você pensou?");
        Caracteristicas cara = new CaracteristicaGenerica(c);
        adicionaAnimalComNovaCategoria(cara);
    }

    private void adicionaAnimal(Caracteristicas c){
        String res = JOptionPane.showInputDialog("Qual nome do seu animal?");
        List<Animal> animais = lista.get(c);
        animais.add(new AnimalGenerico(res, c));
        lista.put(c, animais);
        JOptionPane.showMessageDialog(null, "Animal adicionado!");
    }
    
    private boolean buscaPorAnimal(Caracteristicas c){
        boolean teste = true;
        List<Animal> animais = lista.get(c);
        for(Animal a : animais){
            int res2 = JOptionPane.showConfirmDialog(null, "Seu animal é o "+a.getNome()+"?");
            if(res2 == JOptionPane.YES_OPTION){
                teste = false;
                JOptionPane.showMessageDialog(null, "Acertamos seu animal!");
                return true;
            }
        }
        if(teste){
            adicionaAnimal(c);
            return true;
        }
        return false;
    }

    private void adicionaAnimalComNovaCategoria(Caracteristicas cara) {
        String res = JOptionPane.showInputDialog("Qual nome do seu animal?");
        List<Animal> animais = new ArrayList<Animal>();
        animais.add(new AnimalGenerico(res, cara));
        lista.put(cara, animais);
        JOptionPane.showMessageDialog(null, "Animal adicionado!");
    }
}
