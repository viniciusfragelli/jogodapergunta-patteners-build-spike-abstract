/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jogopergunta;

import Animal.Animal;
import Caracteristicas.Caracteristicas;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author vinicius
 */
public class Controle {
    
    public List<Caracteristicas> carregaListaCaracteristicas(){
        List<Caracteristicas> lista = new ArrayList<Caracteristicas>();
        try{
            File dir = new File("build\\classes\\Caracteristicas\\");
            Caracteristicas cara;
            for(File f : dir.listFiles()){
                if(f.getName().endsWith(".class")){
                    String name=f.getName().substring(0,f.getName().length()-6);
                    //System.out.println("nome "+name);
                    Class klass = Class.forName("Caracteristicas."+name);
                    if(!klass.getSimpleName().equals("Caracteristicas"))
                    {
                        cara = (Caracteristicas) klass.newInstance();
                        lista.add(cara);
                    }
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return lista;
    }
    
    public HashMap<Caracteristicas,List<Animal>> getListaRelativa(){
        List<Caracteristicas> cara = carregaListaCaracteristicas();
        HashMap<Caracteristicas,List<Animal>> lista = new HashMap<Caracteristicas, List<Animal>>();
        try{
            for(Caracteristicas c : cara){
                List<Animal> animal = new ArrayList<Animal>();
                File dir = new File("build\\classes\\Animal\\");
                for(File f : dir.listFiles()){
                    if(f.getName().endsWith(".class")){
                        String name=f.getName().substring(0,f.getName().length()-6);
                        //System.out.println("nome "+name);
                        Class klass = Class.forName("Animal."+name);
                        if(!klass.getSimpleName().equals("Animal"))
                        {
                            Animal a = (Animal) klass.newInstance();
                            Class classe = c.getClass();
                            if(a.getCaracteristicas().getClass().getSimpleName().equals(c.getClass().getSimpleName())){
                                animal.add(a);
                            }
                        }
                    }
                }
                lista.put(c, animal);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return lista;
    } 
    
}
