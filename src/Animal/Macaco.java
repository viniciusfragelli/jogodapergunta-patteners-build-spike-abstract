/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Animal;

import Caracteristicas.Caracteristicas;
import Caracteristicas.Primata;

/**
 *
 * @author vinicius
 */
public class Macaco extends Animal{

    public Macaco() {
        caracteristicas = new Primata();
    }
    
    @Override
    public String getNome() {
        return "MACACO";
    }

    @Override
    public Caracteristicas getCaracteristicas() {
        return caracteristicas;
    }

    @Override
    public void setCaracteristica(Caracteristicas c) {
        caracteristicas = c;
    }
    
}
