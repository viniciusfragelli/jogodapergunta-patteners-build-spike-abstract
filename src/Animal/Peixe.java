/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Animal;

import Caracteristicas.Aquatico;
import Caracteristicas.Caracteristicas;

/**
 *
 * @author vinicius
 */
public class Peixe extends Animal{

    public Peixe() {
        caracteristicas = new Aquatico();
    }
    
    @Override
    public String getNome() {
        return "Peixe";
    }

    @Override
    public Caracteristicas getCaracteristicas() {
        return caracteristicas;
    }

    @Override
    public void setCaracteristica(Caracteristicas c) {
        caracteristicas = c;
    }
    
}
