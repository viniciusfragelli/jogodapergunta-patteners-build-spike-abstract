/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Animal;

import Caracteristicas.Caracteristicas;

/**
 *
 * @author vinicius
 */
public abstract class Animal {
    
    protected Caracteristicas caracteristicas;
    
    public abstract String getNome();
    public abstract Caracteristicas getCaracteristicas();
    public abstract void setCaracteristica(Caracteristicas c);
}
