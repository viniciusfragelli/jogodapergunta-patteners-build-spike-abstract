/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Generico;

import Animal.Animal;
import Caracteristicas.Caracteristicas;

/**
 *
 * @author vinicius
 */
public class AnimalGenerico extends Animal{
    
    private String nome;

    public AnimalGenerico(String nome, Caracteristicas c) {
        this.nome = nome;
        caracteristicas = c;
    }

    @Override
    public String getNome() {
        return nome;
    }

    @Override
    public Caracteristicas getCaracteristicas() {
        return caracteristicas;
    }

    @Override
    public void setCaracteristica(Caracteristicas c) {
        caracteristicas = c;
    }
    
}
