/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Generico;

/**
 *
 * @author vinicius
 */
public class CaracteristicaGenerica extends Caracteristicas.Caracteristicas{

    private String cara;

    public CaracteristicaGenerica(String cara) {
        this.cara = cara;
    }
    
    @Override
    public String caracteristicas() {
        return cara;
    }
    
    public void setCaracteristica(String c){
        cara = c;
    }
}
